<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!-- BEGIN SIDEBAR -->
<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<c:url value='/media/img/user2-160x160.jpg'></c:url>" class="img-circle"
					alt="User Image" />
			</div>
			<div class="pull-left info">
				<p>${sessionScope.username }</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form (Optional) -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control"
					placeholder="Search..." /> <span class="input-group-btn">
					<button type="submit" name="search" id="search-btn"
						class="btn btn-flat">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li class="header">控制板面</li>
			<!-- Optionally, you can add icons to the links -->
			<c:if test="${fn:contains(sessionScope.roles, '管理员') }">
				<li class="active">
					<a href="#"><i class="fa fa-link"></i>
						<span>系统管理</span> 
						<i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li><a href="<c:url value='/authorizations'></c:url>">资源管理</a></li>
						<li><a href="<c:url value='/roles'></c:url>">角色管理</a></li>
						<li><a href="<c:url value='/accounts'></c:url>">账户管理</a></li>
					</ul>
				</li>
			</c:if>
			<c:if test="${fn:contains(sessionScope.roles, '运营') }">
				<li class="treeview">
						<a href="#"><i class="fa fa-link"></i>
							<span>广告管理</span> 
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="<c:url value='/materiels'></c:url>">物料管理</a></li>
							<li><a href="<c:url value='/ads'></c:url>">广告管理</a></li>
						</ul>
				</li>
			</c:if>
			<c:if test="${fn:contains(sessionScope.roles, '商务') }">
				<li class="treeview">
						<a href="#"><i class="fa fa-link"></i>
							<span>报表管理</span> 
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="<c:url value='/report'></c:url>">对账报表</a></li>
						</ul>
				</li>
			</c:if>
		</ul>
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>