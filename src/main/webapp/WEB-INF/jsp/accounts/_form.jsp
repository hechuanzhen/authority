<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">姓名</label>
  <div class="col-sm-10">
    <form:input path="name" class="form-control" placeholder="姓名"  />
  </div>
</div>

<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">登陆邮箱</label>
  <div class="col-sm-10">
    <form:input type="email" path="email" class="form-control" placeholder="xxx@tigerjoys.com"  />
  </div>
</div>

<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">密码</label>
  <div class="col-sm-10">
    <form:input type="password" path="password" class="form-control" placeholder="********" />
  </div>
</div>
                    
<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">角色</label>
  <div class="col-sm-10">
    <div class="checkbox">
		<c:forEach var="role" items="${roles}">
			<label>
        		<form:checkbox path="roleIds" value="${role.id }" />${role.name}
        	</label>
	   </c:forEach>
    </div>
  </div>
</div>
<div class="box-footer">
	<button type="submit" class="btn btn-primary">保存</button>
	<a class="btn btn-default" href="<c:url value='/authorizations'></c:url>">取消</a>
</div>
