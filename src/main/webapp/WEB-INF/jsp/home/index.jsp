<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../_meta.jsp"%>
</head>
<!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <!-- hreader -->
	 <%@ include file="../_nav.jsp" %>
	 <!-- menu -->
	 <%@ include file="../_menu.jsp" %>
	  
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Welcome , 
            <small>${sessionScope.username }</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <section class="content">
        </section>
      </div>
      <!-- /.content-wrapper -->
      
       <!-- Main Footer -->
       <%@ include file="../_footer.jsp" %>
	 </div>
</body>