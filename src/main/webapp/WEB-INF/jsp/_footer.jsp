<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">Anything you want</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2015 <a href="#">Company</a>.
	</strong> All rights reserved.
</footer>
<!-- base js -->
<script src="<c:url value='/media/js/jQuery-2.1.4.min.js'></c:url>" type="text/javascript"></script>
<script src="<c:url value='/media/js/bootstrap.min.js'></c:url>" type="text/javascript"></script>
<script src="<c:url value='/media/js/app.min.js'></c:url>" type="text/javascript"></script>
<!-- data tables -->
<script src="<c:url value='/media/js/jquery.dataTables.min.js'></c:url>" type="text/javascript"></script>
<script src="<c:url value='/media/js/dataTables.bootstrap.min.js'></c:url>" type="text/javascript"></script>


