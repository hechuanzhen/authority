<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="_meta.jsp"%>
</head>
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <!-- hreader -->
	 <%@ include file="_nav.jsp" %>
	 <!-- menu -->
	 <%@ include file="_menu.jsp" %>
	  
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
           哎呀，出错啦，请联系系统管理员。
          </h1>
        </section>
        <section class="content">
        </section>
      </div>
      <!-- /.content-wrapper -->
      
       <!-- Main Footer -->
       <%@ include file="_footer.jsp" %>
	 </div>
</body>