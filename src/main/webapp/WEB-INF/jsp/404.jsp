<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="_meta.jsp"%>
</head>
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <!-- hreader -->
	 <%@ include file="_nav.jsp" %>
	 <!-- menu -->
	 <%@ include file="_menu.jsp" %>
	  
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
           您要的页面找不到了。
          </h1>
        </section>
        <section class="content">
        </section>
      </div>
      <!-- /.content-wrapper -->
      
       <!-- Main Footer -->
       <%@ include file="_footer.jsp" %>
	 </div>
</body>