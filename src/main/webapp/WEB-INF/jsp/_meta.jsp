<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>广告运营平台</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- base css -->
	<link href="<c:url value='/media/css/bootstrap.min.css'></c:url>" rel="stylesheet" type="text/css" />
    <link href="<c:url value='/media/css/font-awesome.min.css'></c:url>" rel="stylesheet" type="text/css" />
	<link href="<c:url value='/media/css/ionicons.min.css'></c:url>" rel="stylesheet" type="text/css" />
	<link href="<c:url value='/media/css/skins/skin-blue.min.css'></c:url>" rel="stylesheet" type="text/css" />
	<link href="<c:url value='/media/css/AdminLTE.min.css'></c:url>" rel="stylesheet" type="text/css" />
	<!-- data tables -->
	<link href="<c:url value='/media/css/dataTables.bootstrap.css'></c:url>" rel="stylesheet" type="text/css" />
