<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">路 径</label>
  <div class="col-sm-10">
    <form:input path="path" class="form-control" placeholder="/xxxx.*" />
  </div>
</div>
<div class="box-footer">
	<button type="submit" class="btn btn-primary">保存</button>
	<a class="btn btn-default" href="<c:url value='/authorizations'></c:url>">取消</a>
</div>
