<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../_meta.jsp"%>
</head>
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <%@ include file="../_nav.jsp" %>
	 <%@ include file="../_menu.jsp" %>
	 
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content">
        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">添加资源</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	  <form:form id="form_sample_2" acceptCharset="UTF-8" cssClass="form-horizontal" commandName="authorization">
                	  	<%@ include file="_form.jsp"%>
                	  </form:form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
        </section>
      </div>
      <!-- /.content-wrapper -->
   
      <!-- Main Footer -->
      <%@ include file="../_footer.jsp" %>
	 </div>
</body>