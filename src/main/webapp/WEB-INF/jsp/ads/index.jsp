<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../_meta.jsp"%>
</head>
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <!-- hreader -->
	 <%@ include file="../_nav.jsp" %>
	 <!-- menu -->
	 <%@ include file="../_menu.jsp" %>
	  
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Welcome , 
            <small>${sessionScope.username }</small>
             ${msg }
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>
        <section class="content">
        </section>
      </div>
      <!-- /.content-wrapper -->
      
       <!-- Main Footer -->
       <%@ include file="../_footer.jsp" %>
	 </div>
</body>