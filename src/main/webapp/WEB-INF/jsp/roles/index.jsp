<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../_meta.jsp"%>
</head>
<body class="skin-blue layout-boxed">
	 <div class="wrapper">
	 <%@ include file="../_nav.jsp" %>
	 <%@ include file="../_menu.jsp" %>
	 
	  <!-- Content Wrapper 正文 -->
      <div class="content-wrapper">
        <section class="content">
        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">角色列表</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                	<div class="pull-right">
						<a href="<c:url value='/roles/new'></c:url>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i>新建</a>
					</div>
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>名称</th>
                        <th>操作</th>
                      </tr>
                    </thead>
                    <tbody>
                    	<c:forEach var="role" items="${ roles}">
	                      <tr>
	                        <td>${role.id }</td>
	                        <td>${role.name }</td>
	                        <td>
	                        	<a href="<c:url value='/roles/edit/${role.id }'></c:url>" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i> 编辑</a>
						        <a href="<c:url value='/roles/delete/${role.id }'></c:url>" class="btn btn-primary btn-sm btn-flat" ><i class="fa fa-remove"></i> 删除</a>
	                        </td>
	                      </tr>
	                     </c:forEach>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>名称</th>
                        <th>操作</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
        </section>
      </div>
      <!-- /.content-wrapper -->
   
      <!-- Main Footer -->
      <%@ include file="../_footer.jsp" %>
	 </div>
	 
	  <script type="text/javascript">
      $(function () {
        $("#example1").DataTable();
       /*  $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        }); */
      });
    </script>
</body>