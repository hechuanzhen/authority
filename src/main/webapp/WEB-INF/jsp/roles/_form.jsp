<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">名称</label>
  <div class="col-sm-10">
    <form:input path="name" class="form-control" placeholder="角色名称"  />
  </div>
</div>
                    
<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">资源</label>
  <div class="col-sm-10">
    <div class="checkbox">
		<c:forEach var="auth" items="${authorizations}">
			<label>
        		<form:checkbox path="authorizationIds" value="${auth.id }" />${auth.path}
        	</label>
	   </c:forEach>
    </div>
  </div>
</div>
<div class="box-footer">
	<button type="submit" class="btn btn-primary">保存</button>
	<a class="btn btn-default" href="<c:url value='/authorizations'></c:url>">取消</a>
</div>
