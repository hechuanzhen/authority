package com.tj.authority.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class MenuDisplayInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String path = request.getServletPath();
		if (path != null && !path.isEmpty()) {
			String[] paths = path.split("/");
			request.setAttribute("path", paths[1]);
		}
		return true;
	}
}
