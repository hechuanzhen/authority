package com.tj.authority.interceptors;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tj.authority.service.SessionService;
import com.tj.authority.vo.SessionAttribute;


public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory
			.getLogger(AuthorizationInterceptor.class);

	@Autowired
	private SessionService sessionService;

	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = sessionService.getSession(request);
		if (session == null) {
			throw new RuntimeException("非法登录 session为null");
		}
		Object object = session.getAttribute(SessionAttribute.AUTHS);
		if (object == null) {
			throw new RuntimeException("非法登录,鉴权失败");
		}

		Set<String> auths = (Set<String>) object;
		String path = request.getServletPath();
		if (auths != null) {
			for(String regex : auths) {
				if(path.matches(regex)) {					
					return true;
				}
			}
		}
		response.sendError(403);
		logger.error("用户 " + session.getAttribute(SessionAttribute.USERNAME)
				+ " 尝试访问无权限服务" + path);
		return false;
	}
}
