package com.tj.authority.interceptors;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class DefenderInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory
			.getLogger(DefenderInterceptor.class);

	private static final String filter = "\\?|<|>|\\(|\\)|;|\"|'|&|\\{|\\}|\\^\\$|#\\[\\]";

	private static final Pattern pattern = Pattern.compile(filter);

	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {

		boolean attack = false;
		Map<String, String[]> parameterMap = request.getParameterMap();

		for (String[] values : parameterMap.values()) {
			for (String val : values) {
				if (pattern.matcher(val).find()) {
					log(request, val);
					attack = true;
					break;
				}
			}
		}

		if (attack) {
			response.sendRedirect(request.getContextPath());
		}
		return !attack;
	}

	private void log(HttpServletRequest request, String value) {
		logger.warn("用户触发了输入参数保护机制, method={}, ip={}, requestURI={}, 非法数据＝{}",
				request.getMethod(), request.getRemoteAddr(),
				request.getRequestURI(), value);
	}

}
