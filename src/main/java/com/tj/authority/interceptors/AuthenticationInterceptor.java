package com.tj.authority.interceptors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tj.authority.service.SessionService;
import com.tj.authority.vo.SessionAttribute;


public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private SessionService sessionService;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = sessionService.getSession(request);
		if (session != null
				&& session.getAttribute(SessionAttribute.USERNAME) != null) {
			return true;
		}
		String contextPath = request.getContextPath();
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			System.out.println("1."+cookie.getDomain());
			System.out.println("2."+cookie.getName());
			System.out.println("3."+cookie.getPath());
			System.out.println("4."+cookie.getValue());
		}
		String remoteAddr = request.getRemoteAddr();
		String remoteHost = request.getRemoteHost();
		String remoteUser = request.getRemoteUser();
		String requestURI = request.getRequestURI();
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int serverPort = request.getServerPort();
		System.out.println(contextPath+"---------");
		System.out.println(remoteAddr+"---------");
		System.out.println(remoteHost+"---------");
		System.out.println(remoteUser+"---------");
		System.out.println(requestURI+"---------");
		System.out.println(scheme+"---------");
		System.out.println(serverName+"---------");
		System.out.println(serverPort+"---------");
		
		response.sendRedirect(contextPath);
		return false;
	}
}
