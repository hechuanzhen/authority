package com.tj.authority.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.tj.authority.service.SessionService;
import com.tj.authority.vo.SessionAttribute;


public class OperationLogInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory
			.getLogger(OperationLogInterceptor.class);

	@Autowired
	private SessionService sessionService;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String method = request.getMethod();
		if ("DELETE".equalsIgnoreCase(method)
				|| "POST".equalsIgnoreCase(method)
				|| "PUT".equalsIgnoreCase(method)
				|| "HEAD".equalsIgnoreCase(method)) {
			HttpSession session = sessionService.getSession(request);
			logger.info("用户名＝{},ip={},uri={},method={}",
					session.getAttribute(SessionAttribute.USERNAME),
					request.getRemoteAddr(), request.getRequestURI(), method);
		}
		return true;
	}
}
