package com.tj.authority.service;

import java.util.List;

import com.tj.authority.entity.Role;

public interface RoleService {
	List<Role> getRoles();

	boolean create(Role entity);

	Role getById(int id);

	boolean update(Role entity);

	boolean deleteById(int id);
}