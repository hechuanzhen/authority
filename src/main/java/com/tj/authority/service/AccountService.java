package com.tj.authority.service;

import java.util.List;

import com.tj.authority.entity.Account;

public interface AccountService {
	List<Account> getAccounts();

	Account findAccount(Account account);

	boolean create(Account entity);

	Account getById(int id);

	boolean update(Account entity);

	boolean deleteById(int id);

}
