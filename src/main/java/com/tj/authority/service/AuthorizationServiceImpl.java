package com.tj.authority.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tj.authority.dao.AuthorizationDao;
import com.tj.authority.entity.Authorization;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

	@Autowired
	AuthorizationDao authorizationDao;

	public List<Authorization> getAuthorizations() {
		return authorizationDao.getAllEntities();
	}

	public boolean create(Authorization r) {
		authorizationDao.create(r);
		return true;
	}

	public Authorization getById(int id) {
		return authorizationDao.getEntityById(id);
	}

	public boolean update(Authorization r) {
		authorizationDao.update(r);
		return true;
	}

	public boolean deleteById(int id) {
		return authorizationDao.deleteById(id) > 0 ? true : false;
	}

}
