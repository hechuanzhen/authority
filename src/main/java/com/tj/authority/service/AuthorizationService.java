package com.tj.authority.service;

import java.util.List;

import com.tj.authority.entity.Authorization;

public interface AuthorizationService {
	List<Authorization> getAuthorizations();

	boolean create(Authorization r);

	Authorization getById(int id);

	boolean update(Authorization r);

	boolean deleteById(int id);
}
