package com.tj.authority.service;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.tj.authority.entity.Authorization;
import com.tj.authority.entity.Role;
import com.tj.authority.vo.SessionAttribute;


@Service
public class SessionServiceImpl implements SessionService {

	public void createSession(HttpServletRequest request, String email,
			Set<Role> roles) {
		Set<String> roleNames = new HashSet<String>();
		Set<String> authorizations = new HashSet<String>();
		// 登陆用户的角色
		for (Role role : roles) {
			roleNames.add(role.getName());
			// 登陆用户的权限(访问路径path集合)
			for (Authorization authorization : role.getAuthorizations()) {
				authorizations.add(authorization.getPath());
			}
		}
		HttpSession session = request.getSession();
		session.setAttribute(SessionAttribute.USERNAME, email);
		session.setAttribute(SessionAttribute.ROLES, roleNames);
		session.setAttribute(SessionAttribute.AUTHS,
				authorizations);

	}

	public void deleteSession(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		session.removeAttribute(SessionAttribute.USERNAME);
		session.removeAttribute(SessionAttribute.ROLES);
		session.removeAttribute(SessionAttribute.AUTHS);
		session.invalidate();
	}

	public HttpSession getSession(HttpServletRequest request) {
		return request.getSession(false);
	}

}
