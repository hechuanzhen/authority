package com.tj.authority.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tj.authority.dao.RoleDao;
import com.tj.authority.entity.Role;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	public List<Role> getRoles() {
		return roleDao.getAllEntities();
	}

	public boolean create(Role entity) {
		roleDao.create(entity);
		return true;
	}

	public Role getById(int id) {
		return roleDao.getEntityById(id);
	}

	public boolean update(Role entity) {
		roleDao.update(entity);
		return true;
	}

	public boolean deleteById(int id) {
		return roleDao.deleteById(id) > 0 ? true : false;
	}

}
