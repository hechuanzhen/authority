package com.tj.authority.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tj.authority.dao.AccountDao;
import com.tj.authority.entity.Account;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountDao accountDao;

	public List<Account> getAccounts() {
		return accountDao.getAllEntities();
	}

	public boolean create(Account entity) {
		accountDao.create(entity);
		return true;
	}

	public Account getById(int id) {
		return accountDao.getEntityById(id);
	}

	public boolean update(Account entity) {
		accountDao.update(entity);
		return true;
	}

	public boolean deleteById(int id) {
		return accountDao.deleteById(id) > 0 ? true : false;
	}

	public Account findAccount(Account account) {
		return accountDao.findAccount(account);
	}

}
