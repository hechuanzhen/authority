package com.tj.authority.service;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tj.authority.entity.Role;


public interface SessionService {

	void createSession(HttpServletRequest request, String email, Set<Role> roles);

	void deleteSession(HttpServletRequest request);

	HttpSession getSession(HttpServletRequest request);

}
