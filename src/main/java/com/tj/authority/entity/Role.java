package com.tj.authority.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.tj.authority.vo.BaseModel;

@Entity
@Table(name = "t_role")
public class Role extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1806347850203982910L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Size(min = 1, max = 100)
	private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "t_role_authorization", joinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "authorization_id", referencedColumnName = "id") })
	private Set<Authorization> authorizations;

	@Transient
	private int[] authorizationIds;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Authorization> getAuthorizations() {
		return authorizations;
	}

	public void setAuthorizations(Set<Authorization> authorizations) {
		this.authorizations = authorizations;
	}

	public int[] getAuthorizationIds() {
		return authorizationIds;
	}

	public void setAuthorizationIds(int[] authorizationIds) {
		this.authorizationIds = authorizationIds;
	}

}
