package com.tj.authority.dao;

import com.tj.authority.entity.Account;

public interface AccountDao extends GenericDao<Account, Integer> {
	Account findAccount(Account account);
}
