package com.tj.authority.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;


abstract class GenericDaoImpl<T, ID extends Serializable> implements
		GenericDao<T, ID> {

	private Class<T> type;

	public Class<T> getType() {
		return type;
	}

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		this.type = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	protected EntityManager getEntityManager() {
		return em;
	}

	@Transactional
	public T create(T entity) {
		em.persist(entity);
		return entity;
	}

	@Transactional
	public T update(T entity) {
		return em.merge(entity);
	}

	public T getEntityById(ID id) {
		return em.find(type, id);
	}

	public List<T> getAllEntities() {
		return em.createQuery("from " + type.getName(), type).getResultList();
	}
	
	@Transactional
	public int deleteById(ID id) {
		int deletedCount = em
				.createQuery(
						"delete from " + type.getName() + " where id = :id")
				.setParameter("id", id).executeUpdate();
		return deletedCount;
	}

}
