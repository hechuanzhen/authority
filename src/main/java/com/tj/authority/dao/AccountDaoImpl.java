package com.tj.authority.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tj.authority.entity.Account;

@Repository
public class AccountDaoImpl extends GenericDaoImpl<Account, Integer>
		implements AccountDao {

	public Account findAccount(Account account) {
		String sql = "from Account where email = :email and password = :password and isdeleted=0";
		List<Account> list = getEntityManager().createQuery(sql, Account.class)
				.setParameter("email", account.getEmail())
				.setParameter("password", account.getPassword()).getResultList();
		return list.size() > 0 ? list.get(0) : null;
	}

}
