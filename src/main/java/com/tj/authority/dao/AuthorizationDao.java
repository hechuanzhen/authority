package com.tj.authority.dao;

import com.tj.authority.entity.Authorization;

public interface AuthorizationDao extends
		GenericDao<Authorization, Integer> {

}
