package com.tj.authority.dao;

import com.tj.authority.entity.Role;

public interface RoleDao extends GenericDao<Role, Integer> {

}
