package com.tj.authority.dao;

import org.springframework.stereotype.Repository;

import com.tj.authority.entity.Authorization;

@Repository
public class AuthorizationDaoImpl extends
		GenericDaoImpl<Authorization, Integer> implements
		AuthorizationDao {

}
