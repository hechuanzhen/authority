package com.tj.authority.dao;

import java.io.Serializable;
import java.util.List;

interface GenericDao<T, ID extends Serializable> {

	T create(T entity);

	T update(T entity);

	T getEntityById(ID id);

	int deleteById(ID id);
	
	List<T> getAllEntities();
}
