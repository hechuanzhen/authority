package com.tj.authority.dao;

import org.springframework.stereotype.Repository;

import com.tj.authority.entity.Role;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Integer> implements
		RoleDao {

}
