package com.tj.authority.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tj.authority.entity.Authorization;
import com.tj.authority.entity.Role;
import com.tj.authority.service.AuthorizationService;
import com.tj.authority.service.RoleService;
import com.tj.authority.util.Transformer;
import com.tj.authority.vo.Path;

@Controller
@RequestMapping(Path.ROLES)
public class RoleController {

	private static final Logger logger = LoggerFactory
			.getLogger(RoleController.class);

	@Autowired
	private RoleService roleService;

	@Autowired
	private AuthorizationService authorizationService;

	@RequestMapping(method = RequestMethod.GET)
	public String getRoles(ModelMap model) {
		model.addAttribute("roles", roleService.getRoles());
		return "roles/index";
	}

	@RequestMapping("/new")
	public String add(ModelMap model) {
		model.addAttribute("role", new Role());
		model.addAttribute("authorizations", authorizationService.getAuthorizations());
		return "roles/new";
	}

	@RequestMapping(value = Path.NEW, method = RequestMethod.POST)
	public String create(@ModelAttribute Role role) {
		role.setAuthorizations(Transformer.convertIdsToModels(
				role.getAuthorizationIds(), Authorization.class));
		if (roleService.create(role)) {
			logger.info("save success");
		}
		return "redirect:/roles";
	}

	@RequestMapping(Path.EDIT)
	public String edit(@PathVariable int id, ModelMap model) {
		Role role = roleService.getById(id);
		role.setAuthorizationIds(Transformer.convertModelsToIds(role.getAuthorizations()));
		model.addAttribute("role", role);
		model.addAttribute("authorizations", authorizationService.getAuthorizations());
		return "roles/edit";
	}

	@RequestMapping(value = Path.EDIT, method = RequestMethod.POST)
	public String update(@ModelAttribute Role role) {
		role.setAuthorizations(Transformer.convertIdsToModels(
				role.getAuthorizationIds(), Authorization.class));		
		if (roleService.update(role)) {
			logger.info("save success");
		}
		return "redirect:/roles";
	}

	@RequestMapping(value = Path.DELETE, method = RequestMethod.GET)
	public String delete(@PathVariable int id) {
		if (roleService.deleteById(id)) {
			logger.info("delete success");
		}
		return "redirect:/roles";
	}

}
