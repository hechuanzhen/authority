package com.tj.authority.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tj.authority.entity.Authorization;
import com.tj.authority.service.AuthorizationService;
import com.tj.authority.vo.Path;


@Controller
@RequestMapping(Path.AUTHORIZATIONS)
public class AuthorizationController{

	private static final Logger logger = LoggerFactory
			.getLogger(AuthorizationController.class);

	@Autowired
	private AuthorizationService authorizationService;

	@RequestMapping(method = RequestMethod.GET)
	public String getAuthorizations(ModelMap model) {
		model.addAttribute("authorizations", authorizationService.getAuthorizations());
		return "authorizations/index";
	}

	@RequestMapping(Path.NEW)
	public String add(ModelMap model) {
		model.addAttribute("authorization", new Authorization());
		return "authorizations/new";
	}

	@RequestMapping(value = Path.NEW, method = RequestMethod.POST)
	public String create(@ModelAttribute Authorization authorization) {
		if (authorizationService.create(authorization)) {
			logger.info("save success");
		}
		return "redirect:/authorizations";
	}

	@RequestMapping(Path.EDIT)
	public String edit(@PathVariable int id, ModelMap model) {
		Authorization authorization = authorizationService.getById(id);
		model.addAttribute("authorization", authorization);
		return "authorizations/edit";
	}

	@RequestMapping(value = Path.EDIT, method = RequestMethod.POST)
	public String update(@ModelAttribute Authorization authorization) {
		if (authorizationService.update(authorization)) {
			logger.info("save success");
		}
		return "redirect:/authorizations";
	}

	@RequestMapping(value = Path.DELETE)
	public String delete(@PathVariable int id) {
		if (authorizationService.deleteById(id)) {
			logger.info("delete success");
		}
		return "redirect:/authorizations";
	}
}
