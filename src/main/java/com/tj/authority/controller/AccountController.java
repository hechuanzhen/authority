package com.tj.authority.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tj.authority.entity.Account;
import com.tj.authority.entity.Role;
import com.tj.authority.service.AccountService;
import com.tj.authority.service.RoleService;
import com.tj.authority.util.Transformer;
import com.tj.authority.vo.Path;

@Controller
@RequestMapping(Path.ACCOUNTS)
public class AccountController{

	private static final Logger logger = LoggerFactory
			.getLogger(AccountController.class);

	@Autowired
	private AccountService accountService;

	@Autowired
	private RoleService roleService;

	@RequestMapping(method = RequestMethod.GET)
	public String getAccounts(ModelMap model) {
		model.addAttribute("accounts", accountService.getAccounts());
		return "accounts/index";
	}

	@RequestMapping(Path.NEW)
	public String add(ModelMap model) {
		model.addAttribute("account", new Account());
		model.addAttribute("roles", roleService.getRoles());
		return "accounts/new";
	}

	@RequestMapping(value = Path.NEW, method = RequestMethod.POST)
	public String create(@Valid Account account, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			model.addAttribute("roles", roleService.getRoles());
			return "accounts/new";
		}
		account.setRoleEntities(Transformer.convertIdsToModels(account.getRoleIds(),
				Role.class));
		if (accountService.create(account)) {
			logger.info("save success");
		}
		return "redirect:/accounts";
	}

	@RequestMapping(Path.EDIT)
	public String edit(@PathVariable int id, ModelMap model) {
		Account account = accountService.getById(id);
		account.setRoleIds(Transformer.convertModelsToIds(account.getRoleEntities()));
		model.addAttribute("account", account);
		model.addAttribute("roles", roleService.getRoles());
		return "accounts/edit";
	}

	@RequestMapping(value = Path.EDIT, method = RequestMethod.POST)
	public String update(@Valid Account account) {
		account.setRoleEntities(Transformer.convertIdsToModels(account.getRoleIds(),
				Role.class));
		if (accountService.update(account)) {
			logger.info("save success");
		}
		return "redirect:/accounts";
	}

	@RequestMapping(value = Path.DELETE, method = RequestMethod.DELETE)
	public @ResponseBody String delete(@PathVariable int id) {
		if (accountService.deleteById(id)) {
			logger.info("delete success");
		}
		return "success";
	}

}
