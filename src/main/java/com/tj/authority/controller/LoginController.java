package com.tj.authority.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tj.authority.entity.Account;
import com.tj.authority.service.AccountService;
import com.tj.authority.service.SessionService;

@Controller
public class LoginController {

	private static final Logger logger = LoggerFactory
			.getLogger(LoginController.class);
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private SessionService sessionService;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginUser(@ModelAttribute Account account,HttpServletRequest request, ModelMap map) {
		logger.info("登陆邮箱为[Email={}],密码为[Password={}]", account.getEmail(),
				account.getPassword());
		Account loginAccount = accountService.findAccount(account);
		if (loginAccount == null) {
			map.addAttribute("account", account);
			map.addAttribute("error", "请输入正确的邮箱和密码!");
			return "forward:/index.jsp";
		}
		sessionService.createSession(request, loginAccount.getEmail(), loginAccount.getRoleEntities());
		return "home/index";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request) {
		sessionService.deleteSession(request);
		return "redirect:/";
	}
}
