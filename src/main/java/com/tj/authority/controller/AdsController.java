package com.tj.authority.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tj.authority.vo.Path;

/**
 * 测试类，可删除
 * @author hechuanzhen
 *
 */
@Controller
@RequestMapping(Path.ADS)
public class AdsController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String getAccounts(ModelMap model) {
		model.addAttribute("msg", "测试广告列表。");
		return "ads/index";
	}
	
}
