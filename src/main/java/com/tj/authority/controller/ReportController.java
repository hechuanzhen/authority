package com.tj.authority.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tj.authority.vo.Path;

/**
 * 测试类，可删除
 * 
 * @author hechuanzhen
 *
 */
@Controller
@RequestMapping(Path.REPORT)
public class ReportController {

	@RequestMapping(method = RequestMethod.GET)
	public String getAccounts(ModelMap model) {
		model.addAttribute("msg", "测试对账报表。");
		return "report/index";
	}

}