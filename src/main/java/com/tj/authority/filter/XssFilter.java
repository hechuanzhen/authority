package com.tj.authority.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * 防止跨站脚本攻击
 * @author hechuanzhen
 *
 */
public class XssFilter implements Filter {

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		XssHttpServletRequestWraper xssRequest = new XssHttpServletRequestWraper(
				(HttpServletRequest) request);
		chain.doFilter(xssRequest, response);
	}

	public void init(FilterConfig arg0) throws ServletException {
		
	}

}