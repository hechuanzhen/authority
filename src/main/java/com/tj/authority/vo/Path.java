package com.tj.authority.vo;

public class Path {
	//导航栏
	public static final String ACCOUNTS = "/accounts";
	public static final String ROLES = "/roles";
	public static final String AUTHORIZATIONS = "/authorizations";
	public static final String ADS = "/ads";
	public static final String MATERIELS = "/materiels";
	public static final String REPORT = "/report";
	
	//功能
	public static final String NEW = "/new";
	public static final String EDIT = "/edit/{id}";
	public static final String UPDATE = "/edit";
	public static final String DELETE = "/delete/{id}";

}
