package com.tj.authority.vo;

public class SessionAttribute {
	public static final String USERNAME = "username";
	public static final String ROLES = "roles";
	public static final String AUTHS = "auths";
}
