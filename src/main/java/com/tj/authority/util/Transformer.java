package com.tj.authority.util;

import java.util.HashSet;
import java.util.Set;

import com.tj.authority.vo.BaseModel;

public class Transformer {

	public static <T extends BaseModel> int[] convertModelsToIds(Set<T> models) {
		if (models == null)
			return null;
		Object[] arr = models.toArray();
		int size = models.size();
		int[] ids = new int[size];
		for (int i = 0; i < size; i++) {
			ids[i] = ((BaseModel)arr[i]).getId();
		}
		return ids;
	}

	public static <T extends BaseModel> Set<T> convertIdsToModels(int[] ids,
			Class<T> clazz) {
		if (ids == null)
			return null;
		int size = ids.length;
		Set<T> models = new HashSet<T>(size);
		for (int i = 0; i < size; i++) {
			try {
				T t = clazz.newInstance();
				t.setId(ids[i]);
				models.add(t);
			} catch (Exception e) {
				throw new RuntimeException();
			}
		}
		return models;
	}
}
