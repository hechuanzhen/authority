# README #

这是一个简单的权限管理系统。

### 运用的框架 ###

* JPA 2.0 + Spring 3.2.2 + SpringMvc 3.2.2
* Bootstrap(AdminLTE-2.2.0框架) + H5

### 运用的工具 ###

* mysql-5.6.17-osx10.7-x86_64
* jboss-eap-6.3


### 如何部署该项目? ###

* 首先创建一个authority的空数据库
* cd到authority项目下，执行migration命令
```
#!java

mvn liquibase:update -Ddriver=com.mysql.jdbc.Driver -Durl='jdbc:mysql://127.0.0.1:3306/longan2v?useUnicode=true&characterEncoding=utf8' -Dusername=root -Dpassword=root
```

* 通过Jboss部署发布项目
* 访问 http://localhost:8080/authority/